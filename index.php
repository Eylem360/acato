<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Acato professionals</title>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<?php
/**
 * Created by PhpStorm.
 * User: eylem
 * Date: 21-11-2016
 * Time: 14:54
 */

$file = $_SERVER['DOCUMENT_ROOT'] . "/carrousel.json";
if (!file_exists($file)) {
    $jsonArray = array(
        array('name' => 'Gijs van den Boom', 'image' => 'https://www.acato.nl/dist/assets/images/aca-gijs-van-den-boom.png'),
        array('name' => 'Lodewijk van der Meer', 'image' => 'https://www.acato.nl/dist/assets/images/aca-lodewijk-van-der-meer.png'),
        array('name' => 'Nellie Keijzer', 'image' => 'https://www.acato.nl/dist/assets/images/aca-valentijn-de-jong.png')
    );

    $json = json_encode($jsonArray);
    file_put_contents($file, $json);
}

$jsonString = file_get_contents($file);
$json = json_decode($jsonString); ?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-inner">
        <?php $counter = 0; ?>
        <?php foreach ($json as $employee) { ?>
        <div class="item <?php if ($counter === 0) echo 'active'; ?>">
            <img src="<?php echo $employee->image ?>" alt="<?php echo $employee->name?>">
            <div class="carousel-caption">...</div>
        </div>
            <?php $counter++; ?>
        <?php } ?>
    </ol>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</html>